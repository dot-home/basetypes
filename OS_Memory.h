//********************************************************************************
/*!
\author     Kraemer E
\date       27.01.2019

\file       OS_Memory.h
\brief      Functions to handle dynamic memory creation and access

***********************************************************************************/

#ifndef _OS_MEMORY_H_
#define _OS_MEMORY_H_
    
/********************************* includes **********************************/
#include "BaseTypes.h"
    
/***************************** defines / macros ******************************/

/****************************** type definitions *****************************/
/* Create enum for fifo status */
typedef enum 
{
  eMemory_Error,
  eMemory_OK
}teMemoryResult;

/***************************** global variables ******************************/


/************************ externally visible functions ***********************/
#ifdef __cplusplus
extern "C"
{
#endif

teMemoryResult OS_Memory_Release(u32* pulMemoryAddr, u8 ucMemorySize);
teMemoryResult OS_Memory_Create(u32** pulMemoryAddr, u8 ucMemorySize);
teMemoryResult OS_Memory_Get(u32* pulMemoryAddr, u8* pucData);
teMemoryResult OS_Memory_GetBuffer(u32* pulMemoryAddr, u8* pucData, u8 ucSize);
teMemoryResult OS_Memory_Put(u32* pulMemoryAddr, const u8* pucData);
teMemoryResult OS_Memory_PutBuffer(u32* pulMemoryAddr, const u8* pucData, u8 ucSize);
teMemoryResult OS_Memory_Overwrite(u32* pulMemoryAddr, const u8* pucData, u8 ucSize, u8 ucOffset);


#ifdef __cplusplus
}
#endif

#endif //_OS_MEMORY_H_
