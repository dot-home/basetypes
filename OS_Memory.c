//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021

\file       OS_Memory.c
\brief      Functions to handle heap memory creation and access

***********************************************************************************/
#include <string.h>
#include <stdlib.h>
#include "BaseTypes.h"
#include "OS_Memory.h"
#include "OS_ErrorDebouncer.h"

#include "OS_Serial_UART.h"

/****************************************** Defines ******************************************************/

/****************************************** Variables ****************************************************/
static s32 slUsedHeap = 0;
static s8  scReservedBlockCount = 0;

/****************************************** Function prototypes ******************************************/

/****************************************** local functions *********************************************/


/****************************************** External visible functions **********************************/
//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Releases the reserved memory. Also checks if the released memory
            is allowed to be released.
\return     eResult - eMemory_Error when something was wrong.
\param      pvMemoryAddr - The address for the memory manager.
\param      ucMemorySize - The size of the reserved memory which shall be released.
***********************************************************************************/
teMemoryResult OS_Memory_Release(u32* pulMemoryAddr, u8 ucMemorySize)
{
    teMemoryResult eResult = eMemory_Error;
    
    if(pulMemoryAddr != NULL && ucMemorySize != 0)
    {
        /* Free heap memory */
        free(pulMemoryAddr);
        
        /* Overwrite address. Heap memory is free but still can be accessed */
        pulMemoryAddr = NULL;
        
        /* Change variable values */
        slUsedHeap -= ucMemorySize;
        --scReservedBlockCount;
        
        /* Check if release is correct */
        if(slUsedHeap < 0 || scReservedBlockCount < 0)
        {
            /* Invalid memory assignment */
            OS_ErrorDebouncer_PutErrorInQueue(eOsMemory_InvalidFree);
        }
        else
        {
            eResult = eMemory_OK;
        }
    }
    return eResult;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Reserves the heap memory. Also checks if the reservation was successfully
            by checking the memory address and for overflows.
\return     eResult - eMemory_Error when something was wrong. Otherwise OK
\param      pvMemoryAddr - The address for the memory manager. Will be overwritten
                           from the memory manager. Create only with NULL-Pointer!!! 
\param      ucMemorySize - The size of the memory which shall be reserved.
***********************************************************************************/
teMemoryResult OS_Memory_Create(u32** pulMemoryAddr, u8 ucMemorySize)
{
    teMemoryResult eResult = eMemory_Error;
    
    if(*pulMemoryAddr == NULL && ucMemorySize != 0)
    {
        /* Reserve heap memory */
        *pulMemoryAddr = malloc(ucMemorySize);                

        /* Change variable values */
        slUsedHeap += ucMemorySize;
        ++scReservedBlockCount;
        
        /* Check if reservation is correct */
        if(slUsedHeap < 0 || scReservedBlockCount < 0 || *pulMemoryAddr == NULL)
        {
            #if USE_OS_SERIAL_UART
                OS_Serial_UART_Printf("Memory invalid create");
            #endif
            
            /* Invalid memory assignment */
            OS_ErrorDebouncer_PutErrorInQueue(eOsMemory_InvalidCreate);
        }
        else
        {
            eResult = eMemory_OK;
        }
    }
    return eResult;
}

//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Copies from the heap memory into the given buffer
\return     eResult - eMemory_Error when something was wrong. Otherwise OK
\param      pvMemoryAddr - The address where the data is saved in the heap.
\param      pucData - Pointer to the buffer where the data shall be copied into.
\param      ucSize - The amount of data in bytes which shall be copied.
***********************************************************************************/
teMemoryResult OS_Memory_GetBuffer(u32* pulMemoryAddr, u8* pucData, u8 ucSize)
{
    teMemoryResult eResult = eMemory_Error;
    
    if(pulMemoryAddr && pucData && ucSize)
    {
        /* Copy given size */
        memcpy(pucData, pulMemoryAddr, ucSize);
        eResult = eMemory_OK;
    }
    return eResult;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Gets a single byte from the heap memory.
\return     eResult - eMemory_Error when something was wrong. Otherwise OK
\param      pvMemoryAddr - The address where the data is saved in the heap.
\param      pucData - Pointer to the buffer where the data shall be copied into.
***********************************************************************************/
teMemoryResult OS_Memory_Get(u32* pulMemoryAddr, u8* pucData)
{
    return OS_Memory_GetBuffer(pulMemoryAddr, pucData, 1);
}


//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Copies from the given buffer into the heap memory
\return     eResult - eMemory_Error when something was wrong. Otherwise OK
\param      pvMemoryAddr - The address where the data is shall be saved in the heap.
\param      pucData - Pointer to the buffer where the data shall be copied from.
\param      ucSize - The amount of data in bytes which shall be copied.
***********************************************************************************/
teMemoryResult OS_Memory_PutBuffer(u32* pulMemoryAddr, const u8* pucData, u8 ucSize)
{
    teMemoryResult eResult = eMemory_Error;
    
    if(pulMemoryAddr && pucData && ucSize)
    {
        /* Copy given size */
        memcpy(pulMemoryAddr, pucData, ucSize);
        eResult = eMemory_OK;
    }
    return eResult;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Copies the given data into the heap memory
\return     eResult - eMemory_Error when something was wrong. Otherwise OK
\param      pvMemoryAddr - The address where the data is shall be saved in the heap.
\param      pucData - Pointer to the buffer where the data shall be copied from.
***********************************************************************************/
teMemoryResult OS_Memory_Put(u32* pulMemoryAddr, const u8* pucData)
{
    return OS_Memory_PutBuffer(pulMemoryAddr, pucData, 1);
}


//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Copies from the given buffer into the heap memory with an offset.
\return     eResult - eMemory_Error when something was wrong. Otherwise OK
\param      pvMemoryAddr - The address where the data is shall be saved in the heap.
\param      pucData - Pointer to the buffer where the data shall be copied from.
\param      ucSize - The amount of data in bytes which shall be copied.
***********************************************************************************/
teMemoryResult OS_Memory_Overwrite(u32* pulMemoryAddr, const u8* pucData, u8 ucSize, u8 ucOffset)
{
    teMemoryResult eResult = eMemory_Error;
    
    if(pulMemoryAddr && pucData && ucSize)
    {
        /* Copy given size */
        memcpy((pulMemoryAddr + ucOffset), pucData, ucSize);
        eResult = eMemory_OK;
    }
    return eResult;
}