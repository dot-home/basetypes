//********************************************************************************
/*!
\author     Kraemer E
\date       27.01.2019

\file       RingBuffer.c
\brief      Functions to handle ringbuffer creation and access

***********************************************************************************/
#include <string.h>
#include "BaseTypes.h"
#include "OS_FIFO.h"

/****************************************** Defines ******************************************************/

/****************************************** Variables ****************************************************/

/****************************************** Function prototypes ******************************************/

/****************************************** local functions *********************************************/


/****************************************** External visible functions **********************************/
//********************************************************************************
/*!
\author     Kraemer E
\date       04.02.2019
\brief      Resets the given fifo and link the given buffer with the structure.
\return     bCreateValid - returns true when everything was fine
\param      ucSize - Size of the given array
\param      pucBuffer - Pointer to the given array
\param      psFIFO - Pointer to the given fifo structure.
***********************************************************************************/
bool OS_FIFO_Create(u8 ucSize, tsFIFO* psFIFO, u8* pucBuffer)
{
    bool bCreateValid = false;
    
    /* Check for a valid pointer and for a valid size of the buffer*/
    if(psFIFO && ucSize)
    {        
        /* Clear put and get index */
        psFIFO->ucPutIdx = 0;
        psFIFO->ucGetIdx = 0;
        
        /* Set size of the buffer */
        psFIFO->ucSize = ucSize;
        
        /* Set size of free entries */
        psFIFO->ucFree = psFIFO->ucSize;
        
        /* link the buffer together */
        psFIFO->pucBuffer = pucBuffer;
        
        bCreateValid = true;
    }

    return bCreateValid;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       08.04.2020
\fn         FifoRemovePutValues
\brief      Removes the data by changing the get index to the put index and increment
            the amount of free data.
\return     FIFO_OK
\param      pFifo - Pointer on FIFO
\param      ucCount - Count of bytes to be removed from the start of buffer
***********************************************************************************/
teFifoResult OS_FIFO_RemovePutValues (tsFIFO* pFifo, u8 ucCount)
{
    if(pFifo)
    {
        /* Change the get index to the place of the put index */
        pFifo->ucGetIdx = pFifo->ucPutIdx;

        /* Check if lower border was reached */
    //    if(pFifo->ucGetIdx == 0)
    //    {
    //        /* Boarder was reached. Set to the higher boarder */
    //        pFifo->ucGetIdx = pFifo->ucSize;
    //    }
    //    else
    //    {
    //        --pFifo->ucGetIdx;
    //    }

        pFifo->ucFree += ucCount;
    }

    return eFIFO_OK;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       27.01.2019
\brief      Get single data entry from the FIFO buffer
\return     teFifoResult - Returns an enum for the result 
\param      psFifoStructure - Pointer to a FIFO structure
\param      pucData - Pointer to the variable or array
***********************************************************************************/
teFifoResult OS_FIFO_Get(tsFIFO* psFifoStruct, u8* pucData)
{
    return OS_FIFO_GetBuffer(psFifoStruct, pucData, SINGLE_ENTRY, true);
}


//********************************************************************************
/*!
\author     Kraemer E
\date       27.01.2019
\brief      This function can be used when more than a byte should be read from the
            buffer. Otherwise "FifoGet" should be used.
\return     teFifoResult - Returns an enum for the result
\param      psFifoStruct - Pointer to the given FIFO structure
\param      pucData - Pointer to the given data array which should be saved in the
                      Buffer
\param      ucCount - The count of the data which should be saved.
\param      bUpdateFree - True when a free entry should be used
***********************************************************************************/
teFifoResult OS_FIFO_GetBuffer(tsFIFO* psFifoStruct, u8* pucData, u8 ucCount, bool bUpdateFree)
{
    teFifoResult eResult = eFIFO_Error;
    
    /* Check for a valid pointer */
    if(psFifoStruct && pucData)
    {
        /* Check if count value is valid and also the free size is smaller than the buffer size */
        if(ucCount && (psFifoStruct->ucFree < psFifoStruct->ucSize))
        {
            /* Check if free counter should be used */
            if(bUpdateFree)
            {
                /* Increase free counter by the data counter */
                psFifoStruct->ucFree += ucCount;
            }
            
            /* Read data from buffer while the data counter isn't zero */
            while(ucCount--)
            {
                /* Save data from buffer */
                *pucData = psFifoStruct->pucBuffer[psFifoStruct->ucGetIdx];
                
                /* Increment to the next given entry */
                pucData++;
                psFifoStruct->ucGetIdx++;
                
                /* Check if the Get size is greater than the buffer size */
                if(psFifoStruct->ucGetIdx >= psFifoStruct->ucSize)
                {
                    /* End was reached */
                    psFifoStruct->ucGetIdx = 0;
                }
            }
            
            eResult = eFIFO_OK;
        }
    }

    return eResult;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       27.01.2019
\brief      Put data into a single entry of the FIFO buffer
\return     teFifoResult - Returns an enum for the result
\param      psFifoStructure - Given pointer to the structure
\param      ucData - Data which should be put into the buffer
***********************************************************************************/
teFifoResult OS_FIFO_Put(tsFIFO* psFifoStructure, u8 ucData)
{
    return OS_FIFO_PutBuffer(psFifoStructure, &ucData, SINGLE_ENTRY, true);
}


//********************************************************************************
/*!
\author     Kraemer E
\date       27.01.2019
\brief      This function can be used when more than a byte should be saved in the
            buffer. Otherwise "FifoPut" should be used.
\return     teFifoResult - Returns an enum for the result
\param      psFifoStruct - Pointer to the given FIFO structure
\param      pucData - Pointer to the given data array which should be saved in the
                      Buffer
\param      ucCount - The count of the data which should be saved.
\param      bUpdateFree - True when a free entry should be used
***********************************************************************************/
teFifoResult OS_FIFO_PutBuffer(tsFIFO* psFifoStruct, u8* pucData, u8 ucCount, bool bUpdateFree)
{
    /* Return value is always false at the beginning */
    teFifoResult eResult = eFIFO_Error;
    
    /* Check first for valid pointer */
    if(pucData && psFifoStruct)        
    {
        /* Check if data count is valid and also that the buffer has
           more free entries than the data could use */
        if(ucCount && psFifoStruct->ucFree >= ucCount)
        {
            /* Check if free slot should be used */
            if(bUpdateFree)
            {
                /* Decrease free counter by the data counter */
                psFifoStruct->ucFree -= ucCount;
                
                #if RINGBUFFER_LOG_MIN_FREE == 1
                if(pFifo->nFree < pFifo->nMinFree)
                {
                    pFifo->nMinFree = pFifo->nFree;
                }
                #endif
            }

            /* Put new data into buffer while the data counter isn't zero */
            while(ucCount--)
            {
                /* Put Data into buffer */
                psFifoStruct->pucBuffer[psFifoStruct->ucPutIdx] = *pucData;
                
                /* Increment data and put index */
                psFifoStruct->ucPutIdx++;
                pucData++;
                
                /* Check if put index reached last entry */
                if(psFifoStruct->ucPutIdx >= psFifoStruct->ucSize)
                {
                    /* Set put index back to zero to start again */
                    psFifoStruct->ucPutIdx = 0;
                }
            }
            
            /* Valid result */
            eResult = eFIFO_OK;
        }
    }

    return eResult;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       27.01.2019
\brief      Get only one byte of the fifo struct without changing the origin 
            get index of the buffer.
\return     teFifoResult - Returns an enum for the result
\param      psFifoStruct - Pointer to the given FIFO structure
\param      pucData - Pointer to the given data array which should be saved in the
                      Buffer
***********************************************************************************/
teFifoResult OS_FIFO_Peek(tsFIFO* psFifoStruct, u8* pucData)
{
    return OS_FIFO_PeekBuffer(psFifoStruct, pucData, SINGLE_ENTRY);
}

//********************************************************************************
/*!
\author     Kraemer E
\date       27.01.2019
\brief      This function can be used when more than a byte should be read from the
            buffer without changing the origin  get index of the buffer. 
            Otherwise "FifoPeek" should be used.
\return     teFifoResult - Returns an enum for the result
\param      psFifoStruct - Pointer to the given FIFO structure
\param      pucData - Pointer to the given data array which should be saved in the
                      Buffer
\param      ucCount - Count of data which should be read
***********************************************************************************/
teFifoResult OS_FIFO_PeekBuffer(tsFIFO* psFifoStruct, u8* pucData, u8 ucCount)
{
    /* Return value is always false at the beginning */
    teFifoResult eResult = eFIFO_Error;
    
    /* Check for a valid pointer first */
    if(pucData && psFifoStruct)
    {
        /* Check if count value is valid and also the free size is smaller than the buffer size */
        if(ucCount && psFifoStruct->ucFree < psFifoStruct->ucSize)
        {
            /* Save get index and free counter in local variables */
            u8 ucLocalGetIdx = psFifoStruct->ucGetIdx;
            u8 ucLocalFree = psFifoStruct->ucFree;

            /* Read data from the buffer without changing the origin get index */
            while(ucCount--)
            {
                /* Save the data */
                *pucData = psFifoStruct->pucBuffer[ucLocalGetIdx];
                
                /* Increment to the next array entry */
                ++pucData;
                ++ucLocalGetIdx;
                
                /* Check if local get index reached end of FIFO */
                if (ucLocalGetIdx >= psFifoStruct->ucSize)
                {
                    ucLocalGetIdx = 0;
                }
                
                /* Check if the end of the valid fifo is reached */
                if(++ucLocalFree == psFifoStruct->ucSize)
                {
                    // leave the loop, no more data
                    break;
                }
            }
            eResult = eFIFO_OK;
        }
    }

    return eResult;
}

//********************************************************************************
/*!
\author     Kraemer E
\date       27.01.2019
\brief      This function can be used when more than a byte should be read from the
            buffer without changing the origin  get index of the buffer. 
            Otherwise "FifoPeek" should be used.
\return     teFifoResult - Returns an enum for the result
\param      psFifoStruct - Pointer to the given FIFO structure
\param      ucOffset - Offset count of bytes
\param      pucData - Pointer to the given data array which should be saved in the
                      Buffer
***********************************************************************************/
teFifoResult OS_FIFO_PeekOffset(tsFIFO* psFifoStruct, u8 ucOffset, u8* pucData)
{
    /* Return value is always false at the beginning */
    teFifoResult eResult = eFIFO_Error;
    
    /* Check for valid pointer first */
    if(psFifoStruct && pucData)
    {
        /* Create local used get index */
        u8 ucLocalGetIdx = psFifoStruct->ucGetIdx;        

        /* Check if offset is in a valid range */
        if((psFifoStruct->ucSize - ucOffset) > ucLocalGetIdx)
        {
            ucLocalGetIdx += ucOffset;
        }
        else
        {
            ucLocalGetIdx -= (psFifoStruct->ucSize - ucOffset);
        }

        /* Get the saved value */
        *pucData = psFifoStruct->pucBuffer[ucLocalGetIdx];

        eResult = eFIFO_OK;
    }

    return eResult;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       27.01.2019
\brief      Overwrites the data starting at the current get position.
            The get position will be unchanged.
\return     teFifoResult - Returns an enum for the result
\param      psFifoStruct - Pointer to the given FIFO structure
\param      pucData - Pointer to the given data array which should be saved in the
                      Buffer
\param      ucCount - Count of data which should be read
***********************************************************************************/
teFifoResult OS_FIFO_Overwrite (tsFIFO* psFifoStruct, u8* pucData, u8 ucCount)
{
    /* Return value is always false at the beginning */
    teFifoResult eResult = eFIFO_Error;
    
    /* Check for valid pointer and data counter */
    if(psFifoStruct && pucData && ucCount)
    {
        /* Create local get index */
        u8 ucLocalGetIdx = psFifoStruct->ucGetIdx;

        while(ucCount--)
        {
            /* Overwrite data */
            psFifoStruct->pucBuffer[ucLocalGetIdx] = *pucData;
            
            /* Increment to the next data entry */
            ++ucLocalGetIdx;
            ++pucData;
            
            /* Check if end of FIFO is reached */
            if(ucLocalGetIdx >= psFifoStruct->ucSize)
            {
                ucLocalGetIdx = 0;
            }
        }
        eResult = eFIFO_OK;
    }

    return eResult;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       27.01.2019
\brief      Resets the whole content of a fifo.
\return     void
\param      psFifoStruct - Pointer to the given FIFO structure
***********************************************************************************/
void OS_FIFO_Reset(tsFIFO* psFifoStruct)
{
    if(psFifoStruct)
    {
        memset((void*)(psFifoStruct->pucBuffer), (long int)0, (size_t)psFifoStruct->ucSize);
        psFifoStruct->ucGetIdx = 0;
        psFifoStruct->ucPutIdx = 0;
        psFifoStruct->ucFree = psFifoStruct->ucSize;
    }

    return;
}
