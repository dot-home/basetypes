/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
 */
#ifndef _BASETYPES_H_
#define _BASETYPES_H_

#include "stdint.h"
#include "stdbool.h"
#include "string.h"
#include "TargetConfig.h"

#ifdef __arm__
#include <stdbool.h>
#endif
#ifdef __cplusplus
#include <stdbool.h>
#endif


#ifdef ESPRESSIF_ESP8266
    #include "c_types.h"
#endif //ESPRESSIF_ESP8266

#ifndef _C_TYPES_H_
/* Typedef for variables */
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned long u32;
typedef unsigned long long u64;

typedef signed char s8;
typedef signed short s16;
typedef signed long s32;
#endif

/* Typedef for function callbacks */
typedef void (*pFunction)(void);
typedef void (*pFunctionParamU8)(u8 ucParam);
typedef void (*pFunctionParamVoid)(void* pParam);
typedef u32 (*pulFunction)(void);
typedef void (*pFunctionParamU32)(u32 ulParam);
typedef void (*pFunctionParamU8S16)(uint_fast8_t ucParam, int_fast16_t siParam);

// used to determine the number of elements in an array
#ifndef _countof
#define _countof(x)     (sizeof(x) / sizeof(x[0]))
#endif

#define false 0
#define true  1

#define ON    1
#define OFF   0

#define EVT_PROCESSED       0
#define EVT_NOT_PROCESSED   1

#endif // _BASETYPES_H_
